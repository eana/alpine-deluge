# Docker Deluge - (Based on Alpine Linux)

A docker container for Deluge. It uses Alpine Linux as a base (6mb)

### Build the image
Steps to build the image:

- Build the image with the command below:
```
docker build -t alpine-deluge github.com/eana/alpine-deluge
```

- Run the built image with the command below:
```
docker run -d --name=alpine-deluge -v $(pwd)/config:/config -v /path_to_torrents:/torrents -p 8112:8112 -p 21021-21060:21021-21060 --restart always alpine-deluge
```

# Notes
The default web GUI password is 'deluge'

### Volumes
All torrent data is stored under the '/torrents' directory.
Configuration data is stored under the '/config' directory.
