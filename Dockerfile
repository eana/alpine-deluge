FROM alpine

ADD startdeluge /usr/local/bin/startdeluge
ADD config /config

RUN set -xe && \
    echo "@testing http://nl.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories && \
    apk add --update --no-cache bash py-pip deluge@testing && \
    rm -rf /var/cache/apk/* && \
    pip install --upgrade pip && \
    pip install --upgrade setuptools && \
    pip2 install incremental constantly packaging automat service_identity && \
    chmod 755 /usr/local/bin/startdeluge

CMD ["/usr/local/bin/startdeluge"]
